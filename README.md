A utility docker image with:
- [Ansible](https://www.ansible.com/)
- [Terraform](https://www.terraform.io/)
- [jq](https://stedolan.github.io/jq/)
- [python-ovh](https://github.com/ovh/python-ovh)
